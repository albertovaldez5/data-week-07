# Introduction

Structured Query Language is a language used as a standard way to interact with databases. It is used for the C.R.U.D. operations (Create, Read, Update and Delete) <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> in a database as well as the admin parts of it.

For example, getting customer id and name from the `customer` table.

```sql
SELECT customer_id, first_name FROM customer
         LIMIT 5;
```

<div class="results" id="org58fc835">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">customer_id</th>
<th scope="col" class="org-left">first_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">MARY</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">PATRICIA</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">LINDA</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">BARBARA</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">ELIZABETH</td>
</tr>
</tbody>
</table>

</div>

A more complex query, where we want to get late rentals information.

```sql
SELECT
	CONCAT(customer.last_name, ', ', customer.first_name) AS customer,
	address.phone,
	film.title
FROM
	rental
	INNER JOIN customer ON rental.customer_id = customer.customer_id
	INNER JOIN address ON customer.address_id = address.address_id
	INNER JOIN inventory ON rental.inventory_id = inventory.inventory_id
	INNER JOIN film ON inventory.film_id = film.film_id
WHERE
	rental.return_date IS NULL
	AND rental_date < CURRENT_DATE
ORDER BY
	title
LIMIT 5;
```

<div class="results" id="orgf053472">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">customer</th>
<th scope="col" class="org-right">phone</th>
<th scope="col" class="org-left">title</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">OLVERA, DWAYNE</td>
<td class="org-right">62127829280</td>
<td class="org-left">ACADEMY DINOSAUR</td>
</tr>


<tr>
<td class="org-left">HUEY, BRANDON</td>
<td class="org-right">99883471275</td>
<td class="org-left">ACE GOLDFINGER</td>
</tr>


<tr>
<td class="org-left">OWENS, CARMEN</td>
<td class="org-right">272234298332</td>
<td class="org-left">AFFAIR PREJUDICE</td>
</tr>


<tr>
<td class="org-left">HANNON, SETH</td>
<td class="org-right">864392582257</td>
<td class="org-left">AFRICAN EGG</td>
</tr>


<tr>
<td class="org-left">COLE, TRACY</td>
<td class="org-right">371490777743</td>
<td class="org-left">ALI FOREVER</td>
</tr>
</tbody>
</table>

</div>


# Relational Databases

SQL is the language that was developed to work with relational databases, which are a type of database that stores data points and their relationships between them in order to access them fast and having a single source of truth.

For example, having customer information related to a single customer id stored in different tables with columns that relate to other tables. <sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>

1.  Databases consists of multiple entries.
2.  Highly structured and represented using a schema.
3.  Reduced data redundancy.

They work very well in business settings as they want to keep track of all transaction which can exist in a single table. But they also can access more information about the different agents involved in the transaction by looking up their data in thei respective tables. For example, having customers, employees and transactions tables which relate to each other with customer id, employee id, etc.

We use relational databases management systems (RDMS) like MySQL or Postgresql in order to deal with all the complexity that arises when working with relational databases, and SQL is the language at the center of it.


## Database Normalization

There are a few guidelines we can follow when creating tables for our database. One of them is database normalization, which is the process of organizing data in a relational database, it should help us eliminate redundancy and inconsistent dependency <sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>.

There are a few forms, named &ldquo;normal fonts&rdquo;, which we can use as guide to structure our data <sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup>.


## Entity Relationship Diagram

An ERD is a graphic way to organize databases and their relationships. We can create an ERD with websites like these: <https://www.quickdatabasediagrams.com/>.

![img](../resources/EmployeeDB.png)


## Schemas

A database schema is also another way to organize a relational database, which can be SQL code with table names, fields, data types and relationships. <sup><a id="fnr.5" class="footref" href="#fn.5" role="doc-backlink">5</a></sup> It is a &ldquo;blueprint&rdquo; used to describe the data models in the database. Schemas give us integrity, organization and security.

We can create a schema with SQL by simply writing a few queries to create tables with their respective data types.

```sql
CREATE TABLE IF NOT EXISTS employee (
             employee_id SERIAL NOT NULL,
             first_name VARCHAR(40) NOT NULL,
             age INTEGER NOT NULL,
             email VARCHAR(80),
             PRIMARY KEY (employee_id)
);
```

    CREATE TABLE

The Primary Key will be an unique value for each row of the database. We can have compound primary keys using two or more values. We can also have Foreign Keys which are keys from other tables, this establishes a relationship between the tables.


# Postgresql

Postgresql is one of the most popular and powerful RDMS which is also open source <sup><a id="fnr.6" class="footref" href="#fn.6" role="doc-backlink">6</a></sup>. We can use it with a graphical interface or from the command line.

Postgresql has a command line interface named `psql` which we can use to manage our databases. The structure for `psql` is server -> database -> table. So if we want to query a table in our database, we need to connect to the psql server first, make sure we have the correct permissions and then we can execute commands on our database.

We can create a database from a bash command if we have added psql to our PATH. <sup><a id="fnr.7" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>


## Psql commands

The following commands are made from the command line, which means we need to have `psql` in our PATH.

We can get a list of our existing databases with `--list`.

```shell
psql --list
```

                                          List of databases
         Name      |     Owner     | Encoding |   Collate   |    Ctype    |   Access privileges
    ---------------+---------------+----------+-------------+-------------+-----------------------
     PH-EmployeeDB | albertovaldez | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
     albertovaldez | albertovaldez | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
     movie_data    | albertovaldez | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
     pagila        | albertovaldez | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
     postgres      | postgres      | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
     template0     | postgres      | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
                   |               |          |             |             | postgres=CTc/postgres
     template1     | postgres      | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
                   |               |          |             |             | postgres=CTc/postgres
     tennis_db     | postgres      | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
     testdb        | albertovaldez | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
    (9 rows)

We can remove a database using `dropdb` and create one with `createdb`.

**NOTE**: Careful using the `dropdb` command!

```shell
dropdb testdb && createdb testdb
```

    

Note that when running `psql` by itself we will enter the psql shell allows us to make queries with SQL.


# Basic Queries

We will go over a few queries for CRUD operations.


## Create and Drop Tables

First let&rsquo;s go back to the minimal schema. So we can create a table in our new database.

```sql
CREATE TABLE IF NOT EXISTS department (
             department_id SERIAL PRIMARY KEY,
             department_name VARCHAR(40) NOT NULL,
             UNIQUE(department_name)
);
CREATE TABLE IF NOT EXISTS employee (
             employee_id SERIAL,
             department_id SERIAL,
             first_name VARCHAR(40) NOT NULL,
             age INTEGER NOT NULL,
             PRIMARY KEY (employee_id),
             CONSTRAINT fk_department
                 FOREIGN KEY (department_id)
                 REFERENCES department(department_id)
);
```

    CREATE TABLE
    CREATE TABLE


## Inserting Data

We must populate the department table first as our employees depent on it.

```sql
INSERT INTO department(department_name)
VALUES ('Corporate'),
       ('Sales'),
       ('Accounting'),
       ('Human Resources'),
       ('Warehouse');
```

    INSERT 0 5

We can see that the department\_id is filled automatically because we used the `SERIAL` data type.

```sql
SELECT * FROM department;
```

<div class="results" id="org2a28164">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">department_id</th>
<th scope="col" class="org-left">department_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">Corporate</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">Sales</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">Accounting</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">Warehouse</td>
</tr>
</tbody>
</table>

</div>

We can insert data into the employee table now as the department\_id data exists in our department table.

```sql
INSERT INTO employee(first_name, department_id, age)
VALUES ('Jim', 2, 27),
       ('Michael', 1, 40),
       ('Angela', 3, 32);
```

    INSERT 0 3


## Joins

Finally we can do a `JOIN` to get info from both tables at once.

```sql
SELECT e.first_name, d.department_name FROM employee as e
JOIN department as d
ON e.department_id = d.department_id;
```

<div class="results" id="org5d336c5">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">first_name</th>
<th scope="col" class="org-left">department_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Jim</td>
<td class="org-left">Sales</td>
</tr>


<tr>
<td class="org-left">Michael</td>
<td class="org-left">Corporate</td>
</tr>


<tr>
<td class="org-left">Angela</td>
<td class="org-left">Accounting</td>
</tr>
</tbody>
</table>

</div>


# Loading the Pagila DB

We are going to use the [Pagila Database](https://github.com/devrimgunduz/pagila) for making the following queries.

We can clone our fork of the pagila database in this repository (assuming we are in the root directory of the project).

```shell
git submodule add git@github.com:AlbertoV5/pagila.git
```

We are going to import the data with an SQL dump <sup><a id="fnr.8" class="footref" href="#fn.8" role="doc-backlink">8</a></sup> using psql from the terminal. The install notes are the following:

> The pagila-data.sql file and the pagila-insert-data.sql both contain the same data, the former using COPY commands, the latter using INSERT commands, so you only need to install one of them.


## Loading the Schema

We can start by creating a database with `psql`. Then loading the schema by specifying the database with `-d` and the file with `-f`. Then we will use the `\dt` command to display all tables in the database just to verify that the schema was loaded properly.

```shell
createdb pagila2
```

```shell
psql -d pagila2 -f "../pagila/pagila-schema.sql"
```

```sql
--Display all tables in database (psql)--
\dt
```

    | List of relations |                  |                   |          |
    |-------------------+------------------+-------------------+----------|
    | Schema            | Name             | Type              | Owner    |
    | public            | actor            | table             | postgres |
    | public            | address          | table             | postgres |
    | public            | category         | table             | postgres |
    | public            | city             | table             | postgres |
    | public            | country          | table             | postgres |
    | public            | customer         | table             | postgres |
    | public            | film             | table             | postgres |
    | public            | film_actor       | table             | postgres |
    | public            | film_category    | table             | postgres |
    | public            | inventory        | table             | postgres |
    | public            | language         | table             | postgres |
    | public            | payment          | partitioned table | postgres |
    | public            | payment_p2022_01 | table             | postgres |
    | public            | payment_p2022_02 | table             | postgres |
    | public            | payment_p2022_03 | table             | postgres |
    | public            | payment_p2022_04 | table             | postgres |
    | public            | payment_p2022_05 | table             | postgres |
    | public            | payment_p2022_06 | table             | postgres |
    | public            | payment_p2022_07 | table             | postgres |
    | public            | rental           | table             | postgres |
    | public            | staff            | table             | postgres |
    | public            | store            | table             | postgres |

Now we add the data by restoring the dump file. This may take a while depending on the amount of data.

```shell
psql -d pagila2 < "../pagila/pagila-insert-data.sql"
```


# Intermediate Queries


## Aggregate Functions

Postgres supports a number of aggregate functions <sup><a id="fnr.9" class="footref" href="#fn.9" role="doc-backlink">9</a></sup> but the most common ones we will use are:

1.  AVG
2.  COUNT
3.  MIN
4.  MAX
5.  SUM

We will normally use them with `GROUP BY` and aliases. We will also use them with `HAVING` which filters aggregate values (after aggregation). If we want the Average Temperature of a Temperature column, we can use `AVG` and `HAVING` to deal with multiple values related to a single entity and then filter them out after aggregation.

For a query where we want the count of films grouped by rating, we can use `COUNT` and `GROUP BY`. Rules for aggregation and group by:

1.  Whatever appears in the group by, it should appear in the select.
2.  Whatever appears in the select, is either an aggregation or is also in the group by.

```sql
SELECT rating, COUNT(film_id) as "Total Films"
FROM film
GROUP BY rating;
```

<div class="results" id="org062b37f">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">rating</th>
<th scope="col" class="org-right">Total Films</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">PG-13</td>
<td class="org-right">223</td>
</tr>


<tr>
<td class="org-left">G</td>
<td class="org-right">178</td>
</tr>


<tr>
<td class="org-left">PG</td>
<td class="org-right">194</td>
</tr>


<tr>
<td class="org-left">R</td>
<td class="org-right">195</td>
</tr>


<tr>
<td class="org-left">NC-17</td>
<td class="org-right">210</td>
</tr>
</tbody>
</table>

</div>


## Where and Having

We can combine all aggregation, group by and having in a single query. In this case we want the total films by rating, `having` total film `count` higher than 200,

The order of the following query is executed in this way:

1.  **FROM**: If we have JOINS or sub-queries they would happen here.
2.  **WHERE**: Then the result of the FROM is filtered.
3.  **GROUP BY**: Values get sorted and indexed by the given columns.
4.  **SELECT**: The aggregations are computed.
5.  **HAVING**: We filter the results of the SELECT.

```sql
SELECT rating,
       COUNT(film_id) as "Total Films",
       AVG(rental_duration) as "Rental Duration",
       MIN(rental_rate) as "Minimum Rate"
FROM film
WHERE rating in ('PG-13', 'PG', 'NC-17')
GROUP BY rating
HAVING COUNT(film_id) > 180;
```

<div class="results" id="orga16e8bd">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">rating</th>
<th scope="col" class="org-right">Total Films</th>
<th scope="col" class="org-right">Rental Duration</th>
<th scope="col" class="org-right">Minimum Rate</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">PG-13</td>
<td class="org-right">223</td>
<td class="org-right">5.0538116591928251</td>
<td class="org-right">0.99</td>
</tr>


<tr>
<td class="org-left">PG</td>
<td class="org-right">194</td>
<td class="org-right">5.0824742268041237</td>
<td class="org-right">0.99</td>
</tr>


<tr>
<td class="org-left">NC-17</td>
<td class="org-right">210</td>
<td class="org-right">5.1428571428571429</td>
<td class="org-right">0.99</td>
</tr>
</tbody>
</table>

</div>


## Order By

We can use `ORDER BY` to sort our results and use the keywords `DESC` for descending and `ASC` for ascending.

```sql
SELECT rating, AVG(rental_rate) as "Average Rate"
FROM film
WHERE rating in ('PG', 'PG-13', 'NC-17')
GROUP BY rating
HAVING AVG(rental_rate) > 3
ORDER BY "Average Rate" DESC
LIMIT 1;
```

<div class="results" id="org71e7bab">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">rating</th>
<th scope="col" class="org-right">Average Rate</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">PG</td>
<td class="org-right">3.0518556701030928</td>
</tr>
</tbody>
</table>

</div>

The last thing to happen in the execution will be `LIMIT`, and right before it will be `ORDER BY`. So it makes sense to process all the data, then sort it and then just get the &ldquo;top N&rdquo; results.

For example we can get the Top 5 higher average replacement costs for the length of the movie.

```sql
SELECT length, ROUND(AVG(replacement_cost), 2) AS "Average Cost"
FROM film
GROUP BY length
ORDER BY "Average Cost" DESC
LIMIT 5;
```

<div class="results" id="org77974e7">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">length</th>
<th scope="col" class="org-right">Average Cost</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">177</td>
<td class="org-right">27.32</td>
</tr>


<tr>
<td class="org-right">131</td>
<td class="org-right">25.79</td>
</tr>


<tr>
<td class="org-right">133</td>
<td class="org-right">25.19</td>
</tr>


<tr>
<td class="org-right">62</td>
<td class="org-right">24.16</td>
</tr>


<tr>
<td class="org-right">66</td>
<td class="org-right">23.99</td>
</tr>
</tbody>
</table>

</div>


## Including Joins

We can also include `JOIN` when using aggregate functions. For example if we wanted the `COUNT` of addresses in a country, but our data is spread in two tables, we can perform any number of `INNER JOIN` before we filter and aggregate.

```sql
SELECT country.country, count(address.address_id) as "Country Count"
FROM city
INNER JOIN country ON city.country_id = country.country_id
INNER JOIN address ON city.city_id = address.city_id
GROUP BY country.country
ORDER BY "Country Count" DESC
LIMIT 5;
```

<div class="results" id="org605c1ea">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">country</th>
<th scope="col" class="org-right">Country Count</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">India</td>
<td class="org-right">60</td>
</tr>


<tr>
<td class="org-left">China</td>
<td class="org-right">53</td>
</tr>


<tr>
<td class="org-left">United States</td>
<td class="org-right">36</td>
</tr>


<tr>
<td class="org-left">Japan</td>
<td class="org-right">31</td>
</tr>


<tr>
<td class="org-left">Mexico</td>
<td class="org-right">30</td>
</tr>
</tbody>
</table>

</div>


## Distinct On

In case we don&rsquo;t want repeated data in our query, we can use the `DISTINCT` keyword.

```sql
SELECT DISTINCT customer_id, rental_date
FROM rental
WHERE customer_id = 130
ORDER BY customer_id, rental_date DESC
LIMIT 5;
```

<div class="results" id="org408f3ba">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">customer_id</th>
<th scope="col" class="org-left">rental_date</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">130</td>
<td class="org-left">2022-08-23 07:29:08-05</td>
</tr>


<tr>
<td class="org-right">130</td>
<td class="org-left">2022-08-22 23:29:32-05</td>
</tr>


<tr>
<td class="org-right">130</td>
<td class="org-left">2022-08-20 18:59:01-05</td>
</tr>


<tr>
<td class="org-right">130</td>
<td class="org-left">2022-08-18 17:39:22-05</td>
</tr>


<tr>
<td class="org-right">130</td>
<td class="org-left">2022-08-17 16:31:04-05</td>
</tr>
</tbody>
</table>

</div>

Alternatively, we can use `DISTINCT` on specific columns. This way we can get the latest rental date for each individual customer id.

```sql
SELECT DISTINCT ON (customer_id) customer_id, rental_date
FROM rental
ORDER BY customer_id, rental_date DESC
LIMIT 5;
```

<div class="results" id="org91dfecf">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">customer_id</th>
<th scope="col" class="org-left">rental_date</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">2022-08-22 14:03:46-05</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">2022-08-23 11:39:35-05</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">2022-08-23 01:10:14-05</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">2022-08-23 01:43:00-05</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">2022-08-22 11:37:02-05</td>
</tr>
</tbody>
</table>

</div>


## Subqueries

We can create a subquery within our `WHERE` clause instead of making a `JOIN`. In this case we want to get all film ids from inventory and use that result to filter the film table. This means we are going to find all the films that were never rented because they don&rsquo;t exist in the inventory.

```sql
SELECT film_id, title
FROM film
WHERE film_id NOT IN
    (SELECT film_id FROM inventory)
LIMIT 5;
```

<div class="results" id="org7b5b96c">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">film_id</th>
<th scope="col" class="org-left">title</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">14</td>
<td class="org-left">ALICE FANTASIA</td>
</tr>


<tr>
<td class="org-right">33</td>
<td class="org-left">APOLLO TEEN</td>
</tr>


<tr>
<td class="org-right">36</td>
<td class="org-left">ARGONAUTS TOWN</td>
</tr>


<tr>
<td class="org-right">38</td>
<td class="org-left">ARK RIDGEMONT</td>
</tr>


<tr>
<td class="org-right">41</td>
<td class="org-left">ARSENIC INDEPENDENCE</td>
</tr>
</tbody>
</table>

</div>


# Store Query Results


## Using Into

We can use `INTO` to dump the results of our query into a new table in the database. For example, if we reuse the last example of latest rental per customer, we can include `INTO` to create this new table.

```sql
SELECT DISTINCT ON (customer_id) customer_id, rental_date
INTO latest_rentals
FROM rental
ORDER BY customer_id, rental_date DESC
LIMIT 5;
```

    SELECT 5

Then we can get all the values from the table to confirm the results are the same.

```sql
SELECT * FROM latest_rentals;
```

<div class="results" id="orgc086c24">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">customer_id</th>
<th scope="col" class="org-left">rental_date</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">2022-08-22 14:03:46-05</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">2022-08-23 11:39:35-05</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">2022-08-23 01:10:14-05</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">2022-08-23 01:43:00-05</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">2022-08-22 11:37:02-05</td>
</tr>
</tbody>
</table>

</div>


## Using Views

Views are virtual tables that we can create from queries, they don&rsquo;t exist like the tables made with `INTO` but they are materialized every time the view is referenced in a query.<sup><a id="fnr.10" class="footref" href="#fn.10" role="doc-backlink">10</a></sup>

Views are an easy way to store a query we know is going to be frequently used so we can access it later without the need to create a table from the query.

```sql
DROP VIEW customer_data;
CREATE VIEW customer_data AS
       SELECT customer.customer_id,
              customer.first_name,
              customer.email,
              address.address
       FROM customer
       INNER JOIN address
       ON customer.address_id = address.address_id
       WHERE customer.activebool = true
       LIMIT 5;
```

    DROP VIEW
    CREATE VIEW

```sql
SELECT * FROM customer_data;
```

<div class="results" id="orgee14a82">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">customer_id</th>
<th scope="col" class="org-left">first_name</th>
<th scope="col" class="org-left">email</th>
<th scope="col" class="org-left">address</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">MARY</td>
<td class="org-left">MARY.SMITH@sakilacustomer.org</td>
<td class="org-left">1913 Hanoi Way</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">PATRICIA</td>
<td class="org-left">PATRICIA.JOHNSON@sakilacustomer.org</td>
<td class="org-left">1121 Loja Avenue</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">LINDA</td>
<td class="org-left">LINDA.WILLIAMS@sakilacustomer.org</td>
<td class="org-left">692 Joliet Street</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">BARBARA</td>
<td class="org-left">BARBARA.JONES@sakilacustomer.org</td>
<td class="org-left">1566 Inegl Manor</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">ELIZABETH</td>
<td class="org-left">ELIZABETH.BROWN@sakilacustomer.org</td>
<td class="org-left">53 Idfu Parkway</td>
</tr>
</tbody>
</table>

</div>

Views remain in the dabase after creating them but we can specify views to be temporary by using the `TEMP` keyword. This means they will only remain for the duration of the session.


# Mastering Joins

**NOTE**: This is an extra section on SQL Joins.

We&rsquo;ll go over more `JOINS` in a new sample database: <https://www.coursera.org/projects/mastering-sql-joins>.


## Setup the Database

Basic steps for loading database into server via command line.

```shell
createdb employees
```

```shell
psql -d employees -f "../resources/Mastering_SQL_Joins.sql"
```

```shell
psql -d employees < "../resources/EmployeesExcerpt.sql"
```

```sql
\dt
```

<div class="results" id="orge3afab0">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">List of relations</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Schema</td>
<td class="org-left">Name</td>
<td class="org-left">Type</td>
<td class="org-left">Owner</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">departments</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">departments_dup</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">dept_emp</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">dept_manager</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">dept_manager_dup</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">employees</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>


<tr>
<td class="org-left">public</td>
<td class="org-left">salaries</td>
<td class="org-left">table</td>
<td class="org-left">albertovaldez</td>
</tr>
</tbody>
</table>

</div>

```sql
SELECT * from dept_manager_dup
ORDER BY dept_no
LIMIT 5;
```

<div class="results" id="org884e32c">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">emp_no</th>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-right">from_date</th>
<th scope="col" class="org-right">to_date</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">110114</td>
<td class="org-left">d002</td>
<td class="org-right">1989-12-17</td>
<td class="org-right">9999-01-01</td>
</tr>


<tr>
<td class="org-right">110085</td>
<td class="org-left">d002</td>
<td class="org-right">1985-01-01</td>
<td class="org-right">1989-12-17</td>
</tr>


<tr>
<td class="org-right">110183</td>
<td class="org-left">d003</td>
<td class="org-right">1985-01-01</td>
<td class="org-right">1992-03-21</td>
</tr>


<tr>
<td class="org-right">110228</td>
<td class="org-left">d003</td>
<td class="org-right">1992-03-21</td>
<td class="org-right">9999-01-01</td>
</tr>


<tr>
<td class="org-right">110303</td>
<td class="org-left">d004</td>
<td class="org-right">1985-01-01</td>
<td class="org-right">1988-09-09</td>
</tr>
</tbody>
</table>

</div>

Now we can start making our queries.


## Inner Join

Inner Join returns all matching values in both tables, so there are no empty rows.

**Query**: All managers&rsquo; employees number, dp number and dp name. Order by manager dp number.

```sql
SELECT m.emp_no, m.dept_no, d.dept_name
FROM dept_manager_dup m
INNER JOIN departments_dup d
ON m.dept_no = d.dept_no
ORDER BY m.dept_no
LIMIT 5;
```

<div class="results" id="orgea76808">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">emp_no</th>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-left">dept_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">110183</td>
<td class="org-left">d003</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-right">110228</td>
<td class="org-left">d003</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-right">110303</td>
<td class="org-left">d004</td>
<td class="org-left">Production</td>
</tr>


<tr>
<td class="org-right">110344</td>
<td class="org-left">d004</td>
<td class="org-left">Production</td>
</tr>


<tr>
<td class="org-right">110386</td>
<td class="org-left">d004</td>
<td class="org-left">Production</td>
</tr>
</tbody>
</table>

</div>

**Query**: All manager&rsquo;s emp num, first and last name, dept num and hire date.

```sql
SELECT m.emp_no, e.first_name, e.last_name, m.dept_no, e.hire_date
FROM dept_manager_dup m
INNER JOIN employees e
ON m.emp_no = e.emp_no
LIMIT 5;
```

<div class="results" id="orgf179f4b">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">emp_no</th>
<th scope="col" class="org-left">first_name</th>
<th scope="col" class="org-left">last_name</th>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-right">hire_date</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">110085</td>
<td class="org-left">Ebru</td>
<td class="org-left">Alpin</td>
<td class="org-left">d002</td>
<td class="org-right">1985-01-01</td>
</tr>


<tr>
<td class="org-right">110114</td>
<td class="org-left">Isamu</td>
<td class="org-left">Legleitner</td>
<td class="org-left">d002</td>
<td class="org-right">1985-01-14</td>
</tr>


<tr>
<td class="org-right">110183</td>
<td class="org-left">Shirish</td>
<td class="org-left">Ossenbruggen</td>
<td class="org-left">d003</td>
<td class="org-right">1985-01-01</td>
</tr>


<tr>
<td class="org-right">110228</td>
<td class="org-left">Karsten</td>
<td class="org-left">Sigstam</td>
<td class="org-left">d003</td>
<td class="org-right">1985-08-04</td>
</tr>


<tr>
<td class="org-right">110303</td>
<td class="org-left">Krassimir</td>
<td class="org-left">Wegerle</td>
<td class="org-left">d004</td>
<td class="org-right">1985-01-01</td>
</tr>
</tbody>
</table>

</div>


## Left Join

Left Join returns all records in the left table, even if they don&rsquo;t have a match with the right table. Any unmatched value will simply be null.

```sql
SELECT m.dept_no, m.emp_no, d.dept_name
FROM dept_manager_dup m
LEFT JOIN departments_dup d
ON m.dept_no = d.dept_no
LIMIT 5;
```

<div class="results" id="org956a53b">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-right">emp_no</th>
<th scope="col" class="org-left">dept_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">d002</td>
<td class="org-right">110085</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">d002</td>
<td class="org-right">110114</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">d003</td>
<td class="org-right">110183</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-left">d003</td>
<td class="org-right">110228</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-left">d004</td>
<td class="org-right">110303</td>
<td class="org-left">Production</td>
</tr>
</tbody>
</table>

</div>

All empty cells are `null` values! The resulting table has the same amount of rows than the left table.

Note that `LEFT JOIN` is the same as `LEFT OUTER JOIN` because having a one-sided join implies that it is an outer join, whereas an inner join includes both tables.


## Right Join

Simply the opposite of the `LEFT JOIN`, so the order of the tables can remain the same but we will get all rows from the right table and get null values for all those which don&rsquo;t match the left one.

```sql
SELECT m.dept_no, m.emp_no, d.dept_name
FROM dept_manager_dup m
RIGHT JOIN departments_dup d
ON m.dept_no = d.dept_no
LIMIT 5;
```

<div class="results" id="org3864d36">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-right">emp_no</th>
<th scope="col" class="org-left">dept_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">&#xa0;</td>
<td class="org-right">&#xa0;</td>
<td class="org-left">Marketing</td>
</tr>


<tr>
<td class="org-left">d003</td>
<td class="org-right">110183</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-left">d003</td>
<td class="org-right">110228</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-left">d003</td>
<td class="org-right">110228</td>
<td class="org-left">Human Resources</td>
</tr>


<tr>
<td class="org-left">d003</td>
<td class="org-right">110228</td>
<td class="org-left">Human Resources</td>
</tr>
</tbody>
</table>

</div>


## Join and Where

We can combine `JOIN` and `WHERE`.

**Query**: Employee first and last name whose salary is greater than 145000.

```sql
SELECT e.first_name, e.last_name, s.salary
FROM employees e
JOIN salaries s
ON e.emp_no = s.emp_no
WHERE s.salary > 145000;
```

<div class="results" id="org316dafb">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">first_name</th>
<th scope="col" class="org-left">last_name</th>
<th scope="col" class="org-right">salary</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Itzchak</td>
<td class="org-left">Ramaiah</td>
<td class="org-right">145732</td>
</tr>


<tr>
<td class="org-left">Basim</td>
<td class="org-left">Tischendorf</td>
<td class="org-right">145215</td>
</tr>
</tbody>
</table>

</div>

**Query**: All employees no, first and last name, dept\_no and from\_date with last\_name &rsquo;Markovitch&rsquo;.

This query could also be called &ldquo;Find a Markovitch named employee that is a manager, plus the ones that are not&rdquo;.

```sql
SELECT e.emp_no, e.first_name, e.last_name, dm.dept_no, dm.from_date
FROM employees e
LEFT JOIN dept_manager dm
ON e.emp_no = dm.emp_no
WHERE e.last_name = 'Markovitch'
ORDER BY dm.dept_no
LIMIT 5;
```

<div class="results" id="org19fc504">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">emp_no</th>
<th scope="col" class="org-left">first_name</th>
<th scope="col" class="org-left">last_name</th>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-right">from_date</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">110022</td>
<td class="org-left">Margareta</td>
<td class="org-left">Markovitch</td>
<td class="org-left">d001</td>
<td class="org-right">1985-01-01</td>
</tr>


<tr>
<td class="org-right">21545</td>
<td class="org-left">Boguslaw</td>
<td class="org-left">Markovitch</td>
<td class="org-left">&#xa0;</td>
<td class="org-right">&#xa0;</td>
</tr>


<tr>
<td class="org-right">11817</td>
<td class="org-left">Niranjan</td>
<td class="org-left">Markovitch</td>
<td class="org-left">&#xa0;</td>
<td class="org-right">&#xa0;</td>
</tr>


<tr>
<td class="org-right">15392</td>
<td class="org-left">Pradeep</td>
<td class="org-left">Markovitch</td>
<td class="org-left">&#xa0;</td>
<td class="org-right">&#xa0;</td>
</tr>


<tr>
<td class="org-right">10898</td>
<td class="org-left">Munenori</td>
<td class="org-left">Markovitch</td>
<td class="org-left">&#xa0;</td>
<td class="org-right">&#xa0;</td>
</tr>
</tbody>
</table>

</div>


## Aggregation

**Query**: Average salary by gender.

```sql
SELECT e.gender, ROUND(AVG(salary), 2) AS "Average Salary"
FROM employees e
INNER JOIN salaries s
ON e.emp_no = s.emp_no
GROUP BY e.gender;
```

<div class="results" id="org2be3501">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">gender</th>
<th scope="col" class="org-right">Average Salary</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">M</td>
<td class="org-right">63865.65</td>
</tr>


<tr>
<td class="org-left">F</td>
<td class="org-right">64163.84</td>
</tr>
</tbody>
</table>

</div>

**NOTE**: Remember to add a `GROUP BY` alongside the aggregate function and include the grouped column in the `SELECT`.

**Query**: How many managers per gender.

```sql
SELECT e.gender, COUNT(dm.emp_no)
FROM employees e
JOIN dept_manager dm
ON e.emp_no = dm.emp_no
GROUP BY e.gender;
```

<div class="results" id="orgf4c04bc">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">gender</th>
<th scope="col" class="org-right">count</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">M</td>
<td class="org-right">11</td>
</tr>


<tr>
<td class="org-left">F</td>
<td class="org-right">13</td>
</tr>
</tbody>
</table>

</div>


## Join Multiple Tables

We can join multiple tables for more complex queries. We will need to do multiple joins and specify different `ON` clauses using the aliases to reference the tables properly.

**Query**: All manager&rsquo;s first and last name, dept\_no, hire\_date, to\_date and dept name.

```sql
SELECT e.first_name, e.last_name, m.dept_no, e.hire_date, m.to_date, d.dept_name
FROM employees e
JOIN dept_manager m
ON e.emp_no = m.emp_no
JOIN departments d
ON m.dept_no = d.dept_no
LIMIT 5;
```

<div class="results" id="orgbc69803">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">first_name</th>
<th scope="col" class="org-left">last_name</th>
<th scope="col" class="org-left">dept_no</th>
<th scope="col" class="org-right">hire_date</th>
<th scope="col" class="org-right">to_date</th>
<th scope="col" class="org-left">dept_name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Margareta</td>
<td class="org-left">Markovitch</td>
<td class="org-left">d001</td>
<td class="org-right">1985-01-01</td>
<td class="org-right">1991-10-01</td>
<td class="org-left">Marketing</td>
</tr>


<tr>
<td class="org-left">Vishwani</td>
<td class="org-left">Minakawa</td>
<td class="org-left">d001</td>
<td class="org-right">1986-04-12</td>
<td class="org-right">9999-01-01</td>
<td class="org-left">Marketing</td>
</tr>


<tr>
<td class="org-left">Ebru</td>
<td class="org-left">Alpin</td>
<td class="org-left">d002</td>
<td class="org-right">1985-01-01</td>
<td class="org-right">1989-12-17</td>
<td class="org-left">Finance</td>
</tr>


<tr>
<td class="org-left">Isamu</td>
<td class="org-left">Legleitner</td>
<td class="org-left">d002</td>
<td class="org-right">1985-01-14</td>
<td class="org-right">9999-01-01</td>
<td class="org-left">Finance</td>
</tr>


<tr>
<td class="org-left">Shirish</td>
<td class="org-left">Ossenbruggen</td>
<td class="org-left">d003</td>
<td class="org-right">1985-01-01</td>
<td class="org-right">1992-03-21</td>
<td class="org-left">Human Resources</td>
</tr>
</tbody>
</table>

</div>

**Query**: Average salary for each department. Only departments with avg salary greater than 60000.

```sql
SELECT d.dept_name, AVG(salary) AS "Average Salary"
FROM departments d
JOIN dept_emp de
ON d.dept_no = de.dept_no
JOIN salaries s
ON de.emp_no = s.emp_no
GROUP BY d.dept_name
HAVING AVG(salary) > 60000
ORDER BY AVG(salary) DESC;
```

<div class="results" id="org35b6c4b">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">dept_name</th>
<th scope="col" class="org-right">Average Salary</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Sales</td>
<td class="org-right">80864.731148210548</td>
</tr>


<tr>
<td class="org-left">Marketing</td>
<td class="org-right">72451.846877673225</td>
</tr>


<tr>
<td class="org-left">Finance</td>
<td class="org-right">70620.996544471154</td>
</tr>


<tr>
<td class="org-left">Research</td>
<td class="org-right">60312.626700453454</td>
</tr>
</tbody>
</table>

</div>

**NOTE**: We used the department employee table to link departments with salary to complete our query.

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://en.wikipedia.org/wiki/Create,_read,_update_and_delete>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://azure.microsoft.com/en-us/resources/cloud-computing-dictionary/what-is-a-relational-database/#whatis>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <https://learn.microsoft.com/en-us/office/troubleshoot/access/database-normalization-description>

<sup><a id="fn.4" class="footnum" href="#fnr.4">4</a></sup> <https://en.wikipedia.org/wiki/Database_normalization#Normal_forms>

<sup><a id="fn.5" class="footnum" href="#fnr.5">5</a></sup> <https://www.ibm.com/cloud/learn/database-schema>

<sup><a id="fn.6" class="footnum" href="#fnr.6">6</a></sup> <https://www.postgresql.org/>

<sup><a id="fn.7" class="footnum" href="#fnr.7">7</a></sup> <https://www.postgresql.org/docs/current/app-psql.html>

<sup><a id="fn.8" class="footnum" href="#fnr.8">8</a></sup> <https://www.postgresql.org/docs/current/backup-dump.html#BACKUP-DUMP-RESTORE>

<sup><a id="fn.9" class="footnum" href="#fnr.9">9</a></sup> <https://www.postgresql.org/docs/9.5/functions-aggregate.html>

<sup><a id="fn.10" class="footnum" href="#fnr.10">10</a></sup> <https://www.postgresql.org/docs/9.2/sql-createview.html>
